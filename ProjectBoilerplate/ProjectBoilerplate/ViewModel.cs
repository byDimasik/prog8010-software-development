﻿// Dmitrii Shishliannikov

using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using ProjectBoilerplate.Annotations;

namespace ProjectBoilerplate
{
    public class ViewModel : INotifyPropertyChanged
    {
        private decimal _decimalText;

        public decimal DecimalText
        {
            get => _decimalText;
            set
            {
                _decimalText = value;
                Console.WriteLine(_decimalText);
                Console.WriteLine((7f / 2f).ToString("0.00"));
            }
        }

        public BindingList<string> Items { get; set; } = new BindingList<string>() {"a", "b", "c"};

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}