﻿using EmployeeSearchApp.Models;
using EmployeeSearchApp.Utils;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EmployeeSearchApp.Controllers
{
    public class EmployeeController
    {
        private const string TABLE_NAME = "Employee";

        private const string CREATE_TABLE =
            "IF NOT EXISTS(SELECT NAME FROM sysobjects WHERE name = '" + TABLE_NAME + "') " +
                "CREATE TABLE " + TABLE_NAME + " " +
                "(" +
                    "id INT IDENTITY CONSTRAINT Employee_PK PRIMARY KEY," +
                    "name NVARCHAR(50) NOT NULL," +
                    "position NVARCHAR(30)," +
                    "payRate MONEY" +
                ");";

        private const string INSERT_DATA =
            "INSERT INTO " + TABLE_NAME + " VALUES (@Name, @Position, @PayRate);";

        private const string GET_ALL_RECORDS =
            "SELECT id, name, position, payRate FROM " + TABLE_NAME;

        private readonly Database _db = Database.GetDb();

        public EmployeeController()
        {
            InitTable();
        }

        private void InitTable()
        {
            if (_db.TableExists(TABLE_NAME)) return;

            using (var cmd = new SqlCommand(CREATE_TABLE, _db.Connection))
                cmd.ExecuteNonQuery();

            foreach (var e in EmployeeReader.ReadEmployees())
            {
                var cmd = new SqlCommand(INSERT_DATA, _db.Connection);
                cmd.Parameters.AddWithValue("@Name", e.Name);
                cmd.Parameters.AddWithValue("@Position", e.Position);
                cmd.Parameters.AddWithValue("@PayRate", e.PayRate);

                using (cmd)
                    cmd.ExecuteNonQuery();
            }

        }

        public List<Employee> GetAll()
        {
            var result = new List<Employee>();

            using (var reader = new SqlCommand(GET_ALL_RECORDS, _db.Connection).ExecuteReader())
            {
                while (reader.Read())
                {
                    // These fields are NOT NULL, so we can just get the values
                    var id = reader.GetInt32(0);
                    var name = reader.GetString(1);

                    // These are can be NULL
                    var pos = reader.GetOrdinal("position");
                    var rate = reader.GetOrdinal("payRate");

                    result.Add(new Employee
                    {
                        ID = id,
                        Name = name,
                        Position = reader.IsDBNull(pos) ? null : reader.GetString(pos),
                        PayRate = reader.IsDBNull(rate) ? default(decimal?) : reader.GetDecimal(rate)
                    });
                }
            }

            return result;
        }
    }
}