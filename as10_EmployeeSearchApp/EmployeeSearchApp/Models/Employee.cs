﻿namespace EmployeeSearchApp.Models
{
    public class Employee
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
        public decimal? PayRate { get; set; }
    }
}