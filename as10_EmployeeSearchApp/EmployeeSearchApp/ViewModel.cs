﻿using EmployeeSearchApp.Annotations;
using EmployeeSearchApp.Controllers;
using EmployeeSearchApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace EmployeeSearchApp
{
    /// <summary>
    /// This string extension means that we can call our method Contains as a string method
    /// Example:
    ///     "myString".Contains
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// The default Contains method is case sensitive, but we need a case insensitive one
        /// It's just a syntax sugar
        /// </summary>
        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return source?.IndexOf(toCheck, comp) >= 0;
        }
    }

    public class ViewModel : INotifyPropertyChanged
    {
        private readonly List<Employee> _allEmployees;

        private string _namePattern;
        public string NamePattern
        {
            get => _namePattern;
            set { _namePattern = value; OnPropertyChanged("Employees"); }
        }

        // If NamePattern is null or empty return just _allEmployees
        // Otherwise, using FindAll list method and our custom string Contains method
        // return only elements that match the NamePattern
        public List<Employee> Employees =>
            string.IsNullOrEmpty(NamePattern?.Trim()) ? _allEmployees : _allEmployees.FindAll(e => e.Name.Contains(NamePattern, StringComparison.OrdinalIgnoreCase));


        public ViewModel()
        {
            var controller = new EmployeeController();
            _allEmployees = controller.GetAll();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}