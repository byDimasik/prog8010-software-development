﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace CardIdentifier
{
    public class CardButton : Button
    {
        // Class representing a card.
        // A button with an image in Content and with CardName property representing the name of a card
        public string CardName { get; private set; }

        public CardButton(string path)
        {
            CardName = Path.GetFileNameWithoutExtension(path)?.Replace('_', ' ');

            var thickness = new Thickness(10);
            this.Content = new Image
            {
                Source = new BitmapImage(new Uri(path)),
                Margin = thickness
            };
            this.Margin = thickness;
        }
    }
}