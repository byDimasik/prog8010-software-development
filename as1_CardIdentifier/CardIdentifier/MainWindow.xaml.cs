﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using Path = System.IO.Path;

namespace CardIdentifier
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly string PathToCardsDir = Path.Combine(Environment.CurrentDirectory, @"Resources\Poker_Cards");
        private static readonly int CardsAmount = Directory.GetFiles(PathToCardsDir).Length;

        private readonly Dictionary<string, CardButton> _pathToCard = new Dictionary<string, CardButton>();
        private const short NumberOfCards = 5;

        private int _lastCardIndex = 0;

        public MainWindow()
        {
            InitializeComponent();

            for (var i = 0; i < NumberOfCards; i++)
            {
                CardGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            }

            ChooseCards();
        }

        private void ChooseCards()
        {
            // Choose NumberOfCards cards from the PathToCardsDir directory
            // Add these cards to the CardGrid
            // Each call returns the next NumberOfCards cards starting from _lastCardIndex
            CardGrid.Children.Clear();

            var cardPaths = Directory.GetFiles(PathToCardsDir);

            var normalizedIndex = 0;
            for (var i = _lastCardIndex; i < _lastCardIndex + NumberOfCards; i++)
            {
                normalizedIndex = i % CardsAmount;

                CardButton button;
                if (_pathToCard.ContainsKey(cardPaths[normalizedIndex]))
                {
                    button = _pathToCard[cardPaths[normalizedIndex]];
                }
                else
                {
                    // A little optimization
                    // Create a new CardButton only once on the first call
                    // Put it in _pathToCard dictionary and all next calls take the button from the dictionary
                    button = new CardButton(cardPaths[normalizedIndex]);
                    button.Click += CardButton_onClick;

                    _pathToCard.Add(cardPaths[normalizedIndex], button);
                }

                Grid.SetColumn(button, i - _lastCardIndex);
                CardGrid.Children.Add(button);
            }

            _lastCardIndex = normalizedIndex + 1;
        }

        private void CardButton_onClick(object sender, RoutedEventArgs e)
        {
            if (sender is CardButton button)
            {
                CardNameLabel.Content = button.CardName;
            }
        }

        private void ChangeCardsButton_OnClick(object sender, RoutedEventArgs e)
        {
            ChooseCards();
        }
    }
}
