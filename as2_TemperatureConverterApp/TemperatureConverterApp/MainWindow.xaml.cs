﻿using System.Windows;
using System.Windows.Media;

namespace TemperatureConverterApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private delegate bool ConverterFunc(string value, out double result);

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Convert(ConverterFunc converterFunc, string outputFormat = "{0:G}")
        {
            // Convert data from InputBox using converterFunc
            // Place data to OutputLabel according to outputFormat

            var success = converterFunc(InputBox.Text, out var result);

            if (success)
            {
                OutputLabel.Content = string.Format(outputFormat, result);
                OutputLabel.Foreground = Brushes.Black;
                InputBox.Text = double.Parse(InputBox.Text).ToString("G");

                return;
            }

            if (InputBox.IsEmpty())
            {
                OutputLabel.Content = "";
                return;
            }

            OutputLabel.Content = "Invalid Input Data";
            OutputLabel.Foreground = Brushes.Red;
        }

        private void ToCelsiusButton_OnClick(object sender, RoutedEventArgs e)
        {
            Convert(TemperatureConverter.ToCelsius, "{0:G7} °C");
        }

        private void ToFahrenheitButton_OnClick(object sender, RoutedEventArgs e)
        {
            Convert(TemperatureConverter.ToFahrenheit, "{0:G7} °F");
        }
    }
}
