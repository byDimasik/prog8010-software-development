﻿using System;

namespace TemperatureConverterApp
{
    public class TemperatureConverter
    {
        private static bool FromString(string value, out double result, Func<double, double> converterFunc)
        {
            // Parse data string into double and convert it to Celsius/Fahrenheit using converterFunc 
            // Return false if input data is not a number

            var successParse = double.TryParse(value, out var parsed);

            result = converterFunc(parsed);

            return successParse;
        }

        public static double ToCelsius(double fahrenheit)
        {
            return (5.0 / 9.0) * (fahrenheit - 32.0);
        }

        public static bool ToCelsius(string fahrenheit, out double result)
        {
            return FromString(fahrenheit, out result, ToCelsius);
        }

        public static double ToFahrenheit(double celsius)
        {
            return (9.0 / 5.0) * celsius + 32;
        }

        public static bool ToFahrenheit(string celsius, out double result)
        {
            return FromString(celsius, out result, ToFahrenheit);
        }
    }
}