﻿using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace TemperatureConverterApp.View
{
    public class HintTextBox : TextBox
    {
        private string _hint;
        public string Hint
        {
            get => _hint;
            set
            {
                _hint = value;
                UpdateHint();
            }
        }

        private readonly Brush _hintBrush = Brushes.Gray;
        private readonly Brush _textBrush = Brushes.Black;

        public HintTextBox() : base()
        {
            Foreground = _hintBrush;
        }

        public HintTextBox(string hint = "") : base()
        {
            Hint = hint;
            Foreground = _hintBrush;
        }

        protected override void OnGotKeyboardFocus(KeyboardFocusChangedEventArgs e)
        {
            base.OnGotKeyboardFocus(e);

            if (Foreground != _hintBrush) return;

            Foreground = _textBrush;
            Text = "";
        }

        private void UpdateHint()
        {
            if (!Text.Trim().Equals("")) return;

            Foreground = _hintBrush;
            Text = Hint;
        }

        protected override void OnLostKeyboardFocus(KeyboardFocusChangedEventArgs e)
        {
            base.OnLostKeyboardFocus(e);

            UpdateHint();
        }

        public bool IsEmpty()
        {
            return Foreground == _hintBrush;
        }
    }
}