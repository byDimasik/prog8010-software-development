﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace NumeralConverterApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly Brush _errorBrush = Brushes.Red;
        private readonly Brush _plainBrush = Brushes.Black;

        private readonly string _toRomanHint = $"Input an integer between {RomanNumeralsConverter.SUPPORTED_MINIMUM} and {RomanNumeralsConverter.SUPPORTED_MAXIMUM}";
        private readonly string _toArabicHint = "Input a roman numeral";

        private bool _toRoman = true;

        public MainWindow()
        {
            InitializeComponent();

            NumeralInput.Hint = _toRomanHint;
        }

        private bool ConvertToRoman()
        {
            if (!int.TryParse(NumeralInput.Text, out var parsed)) 
                return false;

            if (parsed < RomanNumeralsConverter.SUPPORTED_MINIMUM)
            {
                ResultOutput.Content = "Romans didn't know about that";
                return true;
            }

            if (parsed > RomanNumeralsConverter.SUPPORTED_MAXIMUM)
            {
                ResultOutput.Content = "The number is too big";
                return true;
            }

            ResultOutput.Content = RomanNumeralsConverter.ToRoman(parsed);

            return true;
        }

        private bool ConvertToArabic()
        {
            if (string.IsNullOrEmpty(NumeralInput.Text)) 
                return false;
            
            try
            {
                ResultOutput.Content = RomanNumeralsConverter.ToArabic(NumeralInput.Text);
            }
            catch (ArgumentException)
            {
                return false;
            }

            return true;
        }

        private void Convert()
        {
            if (ResultOutput == null) return;

            if (ResultOutput.Foreground == _errorBrush)
                ResultOutput.Foreground = _plainBrush;

            if (_toRoman && ConvertToRoman())
                return;

            if (!_toRoman && ConvertToArabic())
                return;

            if (string.IsNullOrEmpty(NumeralInput.Text.Trim()) || NumeralInput.IsEmpty())
            {
                ResultOutput.Content = "";
                return;
            }

            ResultOutput.Content = "Invalid Data";
            ResultOutput.Foreground = _errorBrush;
        }

        private void NumeralInput_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            Convert();
        }

        private void ConvertMode_OnChecked(object sender, RoutedEventArgs e)
        {
            if (NumeralInput == null) return;

            if (sender.Equals(ToRomanMode))
            {
                _toRoman = true;
                NumeralInput.Hint = _toRomanHint;
            }
            else if (sender.Equals(ToArabicMode))
            {
                _toRoman = false;
                NumeralInput.Hint = _toArabicHint;
            }

            Convert();
        }
    }
}
