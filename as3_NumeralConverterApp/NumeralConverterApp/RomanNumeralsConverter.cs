﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace NumeralConverterApp
{
    public class RomanNumeralsConverter
    {
        public const int SUPPORTED_MAXIMUM = 3999;
        public const int SUPPORTED_MINIMUM = 1;

        private static readonly Regex RomanValidator = new Regex(@"^M{0,3}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$");
        // I stole it from here: https://stackoverflow.com/questions/267399/how-do-you-match-only-valid-roman-numerals-with-a-regular-expression
        // But it was a little wrong, so, I changed it

        private static readonly Dictionary<int, string> ToRomanDictionary = new Dictionary<int, string>
        {
            {    1, "I"  },
            {    4, "IV" },
            {    5, "V"  },
            {    9, "IX" },
            {   10, "X"  },
            {   40, "XL" },
            {   50, "L"  },
            {   90, "XC" },
            {  100, "C"  },
            {  400, "CD" },
            {  500, "D"  },
            {  900, "CM" },
            { 1000, "M"  }
        };

        private static readonly Dictionary<char, int> ToArabicDictionary = new Dictionary<char, int>
        {
            { 'I', 1    },
            { 'V', 5    },
            { 'X', 10   },
            { 'L', 50   },
            { 'C', 100  },
            { 'D', 500  },
            { 'M', 1000 }
        };

        public static string ToRoman(int number)
        {
            if (number < SUPPORTED_MINIMUM || number > SUPPORTED_MAXIMUM)
                throw new ArgumentException($"number must be between {SUPPORTED_MINIMUM} and {SUPPORTED_MAXIMUM}");

            var result = new StringBuilder();

            while (number > 0)
            {
                var largestLess = ToRomanDictionary.Keys.Where(i => number >= i).Max();

                var quotient = Math.DivRem(number, largestLess, out var remainder);

                result.Insert(result.Length, ToRomanDictionary[largestLess], quotient);

                number -= quotient * largestLess;
            }

            return result.ToString();
        }

        public static int ToArabic(string numberString)
        {
            numberString = numberString.ToUpper();

            if (!RomanValidator.IsMatch(numberString)) throw new ArgumentException("Input string is not a valid roman numeral");

            var result = 0;

            try
            {
                var i = 0;
                while (i < numberString.Length)
                {
                    var current = ToArabicDictionary[numberString[i]];

                    if (i + 1 < numberString.Length)
                    {
                        var next = ToArabicDictionary[numberString[i + 1]];

                        if (current >= next)
                        {
                            result += current;
                            i++;
                            continue;
                        }

                        result += next - current;
                        i += 2;
                        continue;
                    }

                    result += current;
                    i++;
                }
            }
            catch (KeyNotFoundException)
            {
                throw new ArgumentException("Each character must be one of these: " +
                                            string.Join(" ", ToArabicDictionary.Keys));
            }

            return result;
        }
    }
}