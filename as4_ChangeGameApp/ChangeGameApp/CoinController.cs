﻿using ChangeGameApp.View;
using System.Linq;

namespace ChangeGameApp
{
    public class CoinController
    {
        private readonly CoinTextBox[] _coinBoxes;
        private const int WINNING_AMOUNT = 100;

        public const int FAIL_RESULT = -1;

        public CoinController(params CoinTextBox[] coinBoxes)
        {
            _coinBoxes = coinBoxes;
        }

        public bool CheckCoins(out int result)
        {
            result = _coinBoxes.Sum(coinBox => coinBox.TotalCoins());
            return result == WINNING_AMOUNT;
        }
    }
}