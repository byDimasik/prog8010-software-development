﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ChangeGameApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly CoinController _coinController;

        private Brush _winBrush = Brushes.LawnGreen;
        private Brush _failBrush = Brushes.Red;
        private Brush _normalBrush = Brushes.Black;

        public MainWindow()
        {
            InitializeComponent();
            _coinController = new CoinController(PenniesInput, NickelsInput, DimesInput, QuartersInput);
        }

        private void HintButton_OnClick(object sender, RoutedEventArgs e)
        {
            _coinController.CheckCoins(out var result);

            if (result == CoinController.FAIL_RESULT) return;

            ResultOutput.Content = $"Current amount: {(float)result / 100:0.00}";
            ResultOutput.Foreground = _normalBrush;

            return;
        }

        private void CoinInputs_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (_coinController == null) return;

            if (_coinController.CheckCoins(out _))
            {
                ResultOutput.Content = "WIN!!!";
                ResultOutput.Foreground = _winBrush;
                return;
            }

            ResultOutput.Content = "FAIL!";
            ResultOutput.Foreground = _failBrush;
        }
    }
}
