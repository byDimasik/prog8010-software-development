﻿using System.Windows.Controls;

namespace ChangeGameApp.View
{
    public enum Coin
    {
        Penny = 1,
        Nickel = 5,
        Dime = 10,
        Quarter = 25
    }

    public class CoinTextBox : TextBox
    {
        public Coin CoinType { set; get; }

        public int TotalCoins()
        {
            if (Text.Trim().Equals("")) return 0;

            return int.TryParse(Text, out var result) ? result * (int)CoinType : 0;
        }

    }
}
