﻿using System.Windows;

namespace ChangeGameApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly VM _vm = new VM();

        public MainWindow()
        {
            InitializeComponent();
            DataContext = _vm;
        }

        private void HintButton_OnClick(object sender, RoutedEventArgs e) => _vm.SetHint();

        private void ResultButton_Click(object sender, RoutedEventArgs e) => _vm.Calculate();

        private void ClearButton_Click(object sender, RoutedEventArgs e) => _vm.Clear();
    }
}
