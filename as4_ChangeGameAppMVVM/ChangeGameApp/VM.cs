﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ChangeGameApp
{
    public enum Coin
    {
        Penny = 1,
        Nickel = 5,
        Dime = 10,
        Quarter = 25
    }

    public class VM : INotifyPropertyChanged
    {
        private const int WINNING_AMOUNT = 100;
        private const string WIN_STATUS = "WIN!!!";
        private const string FAIL_STATUS = "Fail :(";

        private int _pennies;
        private int _nickels;
        private int _dimes;
        private int _quarters;

        private string _gameStatus;
        public string GameStatus
        {
            get => _gameStatus;
            set { _gameStatus = value; OnPropertyChanged(); }
        }

        public string Pennies
        {
            get => _pennies.ToString();
            set { SetCoin(ref _pennies, value); OnPropertyChanged(); }

        }

        public string Nickels
        {
            get => _nickels.ToString();
            set { SetCoin(ref _nickels, value); OnPropertyChanged(); }
        }

        public string Dimes
        {
            get => _dimes.ToString();
            set { SetCoin(ref _dimes, value); OnPropertyChanged(); }
        }

        public string Quarters
        {
            get => _quarters.ToString();
            set { SetCoin(ref _quarters, value); OnPropertyChanged(); }
        }

        private void SetCoin(ref int coin, string value)
        {
            if (!int.TryParse(value, out var parsed) || parsed < 0)
            {
                coin = 0;
                return;
            }

            coin = parsed;

            Calculate();
        }

        public void Calculate() => GameStatus = GetTotal() == WINNING_AMOUNT ? WIN_STATUS : FAIL_STATUS;

        public int GetTotal()
        {
            return int.Parse(Pennies) * (int)Coin.Penny +
                   int.Parse(Nickels) * (int)Coin.Nickel +
                   int.Parse(Dimes) * (int)Coin.Dime +
                   int.Parse(Quarters) * (int)Coin.Quarter;
        }

        public void SetHint() => GameStatus = $"Total: {((float)GetTotal() / 100):0.00}";

        public void Clear()
        {
            Pennies = "0";
            Nickels = "0";
            Dimes = "0";
            Quarters = "0";

            GameStatus = "";
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged([CallerMemberName] string property = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        #endregion
    }
}