﻿using System.ComponentModel;
using System.Windows;

namespace DiceSimulatorApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly ViewModel _vm = new ViewModel();

        public MainWindow()
        {
            InitializeComponent();

            DataContext = _vm;
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e) => _vm.Roll();

        private void MainWindow_OnClosing(object sender, CancelEventArgs e) => _vm.Close();
    }
}
