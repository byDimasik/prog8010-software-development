﻿using System;
using System.IO;

namespace DiceSimulatorApp.Utils
{
    public class OutputWriter : IDisposable
    {
        private bool _disposed;

        private const string APPDATA_PATH = "Dice Simulator";
        private readonly TextWriter _outputWriter;

        public readonly string LogFileName;

        public OutputWriter(string logFileName = "result.txt")
        {
            LogFileName = logFileName;

            var fullPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                APPDATA_PATH);

            if (!Directory.Exists(fullPath))
            {
                Directory.CreateDirectory(fullPath);
            }
            fullPath = Path.Combine(fullPath, LogFileName);

            _outputWriter = new StreamWriter(fullPath, true);
            PrintNewSession();
        }

        ~OutputWriter()
        {
            Dispose(false);
        }

        private void PrintNewSession()
        {
            _outputWriter.WriteLine("***********************************");
            _outputWriter.WriteLine("*********** New Session ***********");
            _outputWriter.WriteLine("***********************************");
            _outputWriter.WriteLine("");
        }

        public void Write(string log)
        {
            if (_disposed) return;

            _outputWriter.Write(log);
        }

        public void WriteLine(string log)
        {
            if (_disposed) return;

            _outputWriter.WriteLine(log);
        }

        public void Flush()
        {
            if (_disposed) return;

            _outputWriter.Flush();
        } 

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;

            if (disposing)
            {
                _outputWriter.WriteLine("");
                _outputWriter.WriteLine("***********************************");
                _outputWriter.WriteLine("");
                _outputWriter.Flush();

                _outputWriter.Close();
            }

            _disposed = true;
        }
    }
}