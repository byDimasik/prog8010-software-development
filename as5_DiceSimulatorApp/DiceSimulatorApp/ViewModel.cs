﻿using DiceSimulatorApp.Utils;
using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;

namespace DiceSimulatorApp
{
    public class Die
    {
        public string PathToImage { get; set; }
        public int Value { get; set; }
    }

    public class ViewModel : INotifyPropertyChanged
    {
        private const short DICE_NUMBER = 2;
        public const short MAX_DIE_VALUE = 6;

        private readonly OutputWriter _outputWriter = new OutputWriter();
        private readonly string _pathToDice = Path.Combine(Environment.CurrentDirectory, @"Resources\Dice");
        private readonly Die[] _dice;
        private readonly Random _random = new Random();

        private string[] _selectedDice;
        public string[] SelectedDice
        {
            get => _selectedDice;
            set { _selectedDice = value; OnPropertyChanged(); }
        }

        private string _totalDice;
        public string TotalDice
        {
            get => _totalDice;
            set { _totalDice = $"Total: {value}"; OnPropertyChanged(); }
        }

        public ViewModel()
        {
            var dicePaths = Directory.GetFiles(_pathToDice);
            if (dicePaths.Length != MAX_DIE_VALUE) throw new InvalidDataException($"There must be {MAX_DIE_VALUE} dice");

            Array.Sort(dicePaths);

            _dice = new Die[dicePaths.Length];
            for (var i = 0; i < _dice.Length; i++)
                _dice[i] = new Die() { PathToImage = dicePaths[i], Value = i + 1 };

            Roll();
        }

        public void Roll()
        {
            var newSelected = new string[DICE_NUMBER];
            var total = 0;

            for (var i = 0; i < DICE_NUMBER; i++)
            {
                var dieIndex = _random.Next(0, _dice.Length - 1);

                newSelected[i] = _dice[dieIndex].PathToImage;
                total += _dice[dieIndex].Value;

                _outputWriter.Write(_dice[dieIndex].Value + (i == DICE_NUMBER - 1 ? "" : " + "));
            }
            _outputWriter.WriteLine($" = {total}");
            _outputWriter.Flush();

            SelectedDice = newSelected;
            TotalDice = total.ToString();
        }

        public void Close() => _outputWriter.Dispose();

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged([CallerMemberName] string property = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        #endregion
    }
}