﻿using System.Windows;

namespace WorldSeriesChampions
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ViewModel _vm = new ViewModel();

        public MainWindow()
        {
            InitializeComponent();

            DataContext = _vm;
        }
    }
}
