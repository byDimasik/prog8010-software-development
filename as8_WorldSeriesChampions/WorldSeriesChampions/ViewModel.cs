﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using WorldSeriesChampions.Annotations;

namespace WorldSeriesChampions
{
    public class ViewModel : INotifyPropertyChanged
    {
        private const string TEAMS_FILE = @"Resources\Teams.txt";
        private const string WINNERS_FILE = @"Resources\WorldSeriesWinners.txt";

        private const int START_YEAR = 1903;
        private readonly int[] MISSED_YEARS = { 1904, 1994 };

        // Team name -> years when the team won
        private readonly Dictionary<string, int[]> _teamsStatistic = new Dictionary<string, int[]>();

        public List<string> TeamsList { get; private set; }

        private int[] _wonYears;
        public int[] WonYears
        {
            get => _wonYears;
            set { _wonYears = value; OnPropertyChanged(); }
        }

        private string _selectedTeam;
        public string SelectedTeam
        {
            get => _selectedTeam;
            set
            {
                _selectedTeam = value;
                UpdateWinner();
            }
        }

        private int _totalWins;
        public int TotalWins
        {
            get => _totalWins;
            private set { _totalWins = value; OnPropertyChanged(); }
        }

        public ViewModel()
        {
            ParseData();
        }

        private void ParseData()
        {
            using (StreamReader teamsReader = new StreamReader(Path.Combine(Environment.CurrentDirectory, TEAMS_FILE)),
                statisticsReader = new StreamReader(Path.Combine(Environment.CurrentDirectory, WINNERS_FILE)))
            {
                // Read all rows from WorldSeriesWinners.txt and put them into array
                // Now, we have the following array:
                // Team:   teamName   teamName   teamName   ...
                // Index:     0          1          2
                // So, we can add 1903 to each index and we will get the year when the team won
                // But we have two years when the championship was not played: 1904, 1994
                // It means that we have to add 1 to each year that is greater or equal to 1904
                // and add an extra 1 to each year that is greater or equal than 1994
                var statistics = statisticsReader.ReadToEnd().Split(new[] { Environment.NewLine }, StringSplitOptions.None);

                while (!teamsReader.EndOfStream)
                {
                    var teamName = teamsReader.ReadLine();
                    if (teamName == null) continue;

                    // Select((t - team name from the statistics array, i - its index) => t == teamName ? ... : -1
                    //
                    //    For all the elements that are equal to the teamName return:
                    //       MISSED_YEARS.Aggregate(START_YEAR + i, (current, missed) => current >= missed ? current + 1 : current)
                    //          add START_YEAR to i (team index)
                    //          then for each year from the MISSED_YEARS array add 1 to the index (current) if it is >= than the missed year (missed)
                    //
                    //    For all other elements return -1
                    //
                    // Where(i => i != -1)
                    // 
                    //    Then, filter all the elements that are equal to -1
                    //
                    // Thus, for the team teamName we have the array of the years when the team won the championship
                    var wonYears = statistics
                        .Select((t, i) => t == teamName ? MISSED_YEARS.Aggregate(START_YEAR + i, (current, missed) => current >= missed ? current + 1 : current) : -1)
                        .Where(i => i != -1)
                        .ToArray();

                    _teamsStatistic.Add(teamName, wonYears);
                }

                TeamsList = _teamsStatistic.Keys.ToList();
            }
        }

        private void UpdateWinner()
        {
            TotalWins = _teamsStatistic[_selectedTeam].Length;
            WonYears = _teamsStatistic[_selectedTeam];
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}