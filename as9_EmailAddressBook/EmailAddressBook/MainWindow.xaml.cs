﻿using EmailAddressBook.View;
using EmailAddressBook.ViewModel;
using System.Windows;
using System.Windows.Controls;

namespace EmailAddressBook
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainVM _vm = new MainVM();
        public MainWindow()
        {
            InitializeComponent();

            DataContext = _vm;
        }

        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var window = new PersonWindow(new PersonVM { Person = _vm.Person }) { Owner = this };
            window.Closed += (o, args) => IsEnabled = true;
            window.Show();

            IsEnabled = false;
        }
    }
}
