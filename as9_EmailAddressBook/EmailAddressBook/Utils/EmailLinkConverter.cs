﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace EmailAddressBook.Utils
{
    public class EmailLinkConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return $"mailto:{value}";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}