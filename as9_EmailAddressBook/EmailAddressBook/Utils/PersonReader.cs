﻿using EmailAddressBook.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace EmailAddressBook.Utils
{
    public class PersonReader
    {
        private const string DATA_FILE = @"Resources\PersonsData.csv";
        private static readonly string DEFAULT_PATH = Path.Combine(Environment.CurrentDirectory, DATA_FILE);

        private const char CSV_SEPARATOR = ',';

        public static List<PersonEntry> ReadPersons()
        {
            var result = new List<PersonEntry>();

            using (var r = new StreamReader(DEFAULT_PATH))
            {
                while (!r.EndOfStream)
                {
                    var data = r.ReadLine().Split(CSV_SEPARATOR).Select(d => d.Trim()).ToArray();

                    result.Add(new PersonEntry { Name = data[0], Email = data[1], Phone = data[2] });
                }
            }

            return result;
        }
    }
}