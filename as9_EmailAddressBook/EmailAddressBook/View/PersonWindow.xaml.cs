﻿using EmailAddressBook.ViewModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Navigation;

namespace EmailAddressBook.View
{
    /// <summary>
    /// Interaction logic for PersonWindow.xaml
    /// </summary>
    public partial class PersonWindow : Window
    {
        private PersonVM _vm;

        public PersonWindow(PersonVM vm)
        {
            InitializeComponent();

            WindowStartupLocation = WindowStartupLocation.CenterOwner;

            _vm = vm;
            DataContext = _vm;
        }

        private void Hyperlink_OnRequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }
    }
}
