﻿using EmailAddressBook.Model;
using EmailAddressBook.Utils;
using EmailAddressBook.ViewModel;
using System.ComponentModel;

namespace EmailAddressBook
{
    public class MainVM : PersonVM
    {
        public BindingList<PersonEntry> Persons { get; set; } = new BindingList<PersonEntry>(PersonReader.ReadPersons());
    }
}