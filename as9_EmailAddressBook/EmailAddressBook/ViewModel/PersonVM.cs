﻿using EmailAddressBook.Annotations;
using EmailAddressBook.Model;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace EmailAddressBook.ViewModel
{
    public class PersonVM : INotifyPropertyChanged
    {
        private PersonEntry _person;

        public PersonEntry Person
        {
            get => _person;
            set { _person = value; OnPropertyChanged(); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}