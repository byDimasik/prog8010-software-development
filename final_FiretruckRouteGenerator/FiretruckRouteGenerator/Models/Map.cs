﻿using System;
using System.Collections.Generic;

namespace FiretruckRouteGenerator.Models
{
    public class AdjCorners
    {
        public int Src { get; set; }
        public int Dst { get; set; }
    }

    public class Map
    {
        public const int FIRESTATION_LOCATION = 1;

        /// <summary>
        /// Adjacency List
        /// Key: Streetcorner
        /// Value: All the streetcorners that are reachable from the key streetcorner
        ///
        /// We need Dictionary, not List, because in a config file some corners may be missed if there are no open streets through those corners
        /// So, we cannot use list index as a corner
        /// </summary>
        private readonly Dictionary<int, List<int>> _map = new Dictionary<int, List<int>>();

        public int FireLocation { get; }
        public int MaximumCorner { get; }

        public Map(int fireLocation, IEnumerable<AdjCorners> adjCorners)
        {
            FireLocation = fireLocation;

            var maxCorner = 0;
            foreach (var c in adjCorners)
            {
                maxCorner = c.Src > maxCorner ? c.Src : maxCorner;
                maxCorner = c.Dst > maxCorner ? c.Dst : maxCorner;

                if (!_map.ContainsKey(c.Src)) _map.Add(c.Src, new List<int>());
                if (!_map.ContainsKey(c.Dst)) _map.Add(c.Dst, new List<int>());

                _map[c.Src].Add(c.Dst);
                _map[c.Dst].Add(c.Src);
            }

            MaximumCorner = maxCorner;
        }

        public IList<int> this[int i] => _map[i];
    }
}