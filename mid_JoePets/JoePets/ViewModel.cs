﻿// Dmitrii Shishliannikov

using JoePets.Annotations;
using JoePets.Utils;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace JoePets
{
    public class ViewModel : INotifyPropertyChanged
    {
        private const decimal CRICKETS_PRICE = 10;
        private const decimal WORMS_PRICE = 20;
        private const decimal MICE_PRICE = 5;
        private const decimal DELIVERY_PRICE = 12;
        private const decimal DELIVERY_FREE_LIMIT = 80;
        private const decimal TAXES = 0.13m;

        private OutputWriter _writer = new OutputWriter();

        private int _crickets;
        public int CricketsKg
        {
            get => _crickets;
            set { _crickets = value; OnPropertyChanged(); }
        }

        private int _worms;
        public int WormsKg
        {
            get => _worms;
            set { _worms = value; OnPropertyChanged(); }
        }

        private int _mice;
        public int MiceKg
        {
            get => _mice;
            set { _mice = value; OnPropertyChanged(); }
        }

        private bool _delivery;
        public bool IncludeDelivery
        {
            get => _delivery;
            set { _delivery = value; OnPropertyChanged(); }
        }

        private decimal _taxes;
        public decimal TaxesAmount
        {
            get => _taxes;
            set { _taxes = value; OnPropertyChanged(); }
        }

        private decimal _total;
        public decimal TotalAmount
        {
            get => _total;
            set { _total = value; OnPropertyChanged(); }
        }

        private void WriteInvoice(bool includeDelivery)
        {
            _writer.WriteLine("");
            _writer.WriteLine("*********** New Invoice ***********");
            _writer.WriteLine("");

            if (CricketsKg > 0)
                _writer.WriteLine($"Crickets {CRICKETS_PRICE:C2}/kg: {CricketsKg} kg SubTotal: {CricketsKg * CRICKETS_PRICE:C2}");

            if (WormsKg > 0)
                _writer.WriteLine($"Worms {WORMS_PRICE:C2}/kg: {WormsKg} kg SubTotal: {WormsKg * WORMS_PRICE:C2}");

            if (MiceKg > 0)
                _writer.WriteLine($"Crickets {MICE_PRICE:C2}/kg: {MiceKg} kg SubTotal: {MiceKg * MICE_PRICE:C2}");

            if (includeDelivery)
                _writer.WriteLine($"Delivery: {DELIVERY_PRICE:C2}");

            _writer.WriteLine($"Taxes: {TaxesAmount:C2}");
            _writer.WriteLine($"Total: {TotalAmount:C2}");
            _writer.Flush();
        }

        public void Calculate()
        {
            var tmpTotal = CricketsKg * CRICKETS_PRICE + WormsKg * WORMS_PRICE + MiceKg * MICE_PRICE;

            var includeDeliveryToInvoice = IncludeDelivery && tmpTotal < DELIVERY_FREE_LIMIT;
            if (includeDeliveryToInvoice)
                tmpTotal += DELIVERY_PRICE;

            TaxesAmount = tmpTotal * TAXES;
            TotalAmount = tmpTotal + TaxesAmount;

            WriteInvoice(includeDeliveryToInvoice);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}