﻿// Dmitrii Shishliannikov

using SequenceGenerator.Annotations;
using SequenceGenerator.Utils;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SequenceGenerator
{
    public class SequenceNumber
    {
        private readonly int _minimumValue;
        private readonly int _maximumValue;

        private int _value;
        public int Value
        {
            get => _value;
            set
            {
                _value = value < _minimumValue ? _minimumValue : value;
                _value = value > _maximumValue ? _maximumValue : value;
            }
        }

        public SequenceNumber(int min = 1, int max = 10)
        {
            _minimumValue = min;
            _maximumValue = max;
        }
    }

    public class ViewModel : INotifyPropertyChanged
    {
        private OutputWriter _writer = new OutputWriter();

        private SequenceNumber _iterations = new SequenceNumber(1, 100);
        public SequenceNumber IterationsNumber
        {
            get => _iterations;
            set { _iterations = value; OnPropertyChanged(); }
        }

        public BindingList<SequenceNumber> SequenceInit { get; set; } = new BindingList<SequenceNumber>() { new SequenceNumber(), new SequenceNumber(), new SequenceNumber() };

        private BindingList<int> _sequence = new BindingList<int>();
        public BindingList<int> SequenceValues
        {
            get => _sequence;
            set { _sequence = value; OnPropertyChanged(); }
        }

        // Initial three number are included in the resulting sequence
        public void Calculate()
        {
            var result = new BindingList<int>();

            _writer.WriteLine("");
            _writer.WriteLine("**********************");
            _writer.WriteLine("");

            _writer.WriteLine($"Initial Numbers: {SequenceInit[0].Value} {SequenceInit[1].Value} {SequenceInit[2].Value}");
            _writer.WriteLine($"Number of iterations: {IterationsNumber.Value}");
            _writer.WriteLine("Resulting Sequence:");

            for (var i = 0; i < 3; i++)
            {
                result.Add(SequenceInit[i].Value);
                _writer.WriteLine(SequenceInit[i].Value.ToString());
            }

            for (var i = 3; i < IterationsNumber.Value; i++)
            {
                var newNumber = result[i - 3] * result[i - 2] - result[i - 1];
                result.Add(newNumber);
                _writer.WriteLine(newNumber.ToString());
            }

            _writer.Flush();
            SequenceValues = result;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}